<?php

namespace Drupal\entity_unified_access\Conditions;

use Drupal\Core\Session\AccountInterface;

final class ConstantCondition extends SingleConditionBase implements ConditionInterface {

  /**
   * The boolean value.
   *
   * @var bool
   */
  protected $value;

  /**
   * Creates a boolean constant condition.
   *
   * @param string $name
   *   The name for introspection.
   * @param bool $value
   *   The boolean value.
   */
  public function __construct($name, $value) {
    parent::__construct($name);
    $this->value = $value;
  }

  /**
   * Convenience constructor.
   *
   * @param string $name
   *   The name for introspection.
   * @param bool $value
   *   The boolean value.
   *
   * @return static
   */
  public static function create($name, $value) {
    return new static($name, $value);
  }

  /**
   * Creates BooleanCondition depending on user permission.
   *
   * @param string $name
   *   The name.
   * @param $permission
   *   The permission to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to check.
   *
   * @return static
   */
  public static function ifPermission($name, $permission, AccountInterface $account) {
    $boolean = $account->hasPermission($permission);
    $condition = static::create($name, $boolean);
    $condition->addCacheContexts(['user.permissions']);
    return $condition;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstantValue() {
    return $this->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isDependentOnThisEntity() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    $booleanAsString = $this->value ? 'TRUE' : 'FALSE';
    return "/*{$this->name}*/ $booleanAsString";
  }

}
