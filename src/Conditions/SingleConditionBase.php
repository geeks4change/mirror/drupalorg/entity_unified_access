<?php

namespace Drupal\entity_unified_access\Conditions;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyTrait;

abstract class SingleConditionBase extends ConditionBase implements RefinableCacheableDependencyInterface {

  use RefinableCacheableDependencyTrait;

  /**
   * Sets whether the condition depends on other entities.
   *
   * Syntactical sugar that makes conditions easier to read.
   *
   * @param string $entityTypeId
   *   The entity type ID.
   * @param string[]|null $bundleIds
   *   The bundle IDs. Defaults to all.
   *
   *  @return $this
   *
   *  @todo Leverage bundle list cache tags once we have them.
   */
  public function addCacheableListDependency($entityTypeId, $bundleIds = NULL) {
    $entityType = \Drupal::entityTypeManager()->getDefinition($entityTypeId);
    $this->addCacheTags($entityType->getListCacheTags());
    $this->addCacheContexts($entityType->getListCacheContexts());
    return $this;
  }

}
