<?php

namespace Drupal\entity_unified_access\Conditions;

abstract class ConditionBase implements ConditionInterface {

  /**
   * The name for introspection.
   *
   * @var string
   */
  protected $name;

  /**
   * Constructs a new ConditionGroup object.
   *
   * @param $name
   *   The name.
   */
  public function __construct($name) {
    $this->name = $name;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->name;
  }

}
