<?php

namespace Drupal\entity_unified_access\Conditions;

/**
 * Represents a single query access condition.
 */
final class FieldCondition extends SingleConditionBase implements ConditionInterface {

  /**
   * The supported operators.
   *
   * @var string[]
   */
  protected static $supportedOperators = [
    '=', '<>', '<', '<=', '>', '>=', 'BETWEEN', 'NOT BETWEEN',
    'IN', 'NOT IN', 'IS NULL', 'IS NOT NULL',
  ];

  /**
   * The field.
   *
   * @var string
   */
  protected $field;

  /**
   * The value.
   *
   * @var mixed
   */
  protected $value;

  /**
   * The operator.
   *
   * @var string
   */
  protected $operator;

  /**
   * Constructs a new Condition object.
   *
   * @param $name
   *   The name.
   * @param string $field
   *   The field, with an optional column name. E.g: 'uid', 'address.locality'.
   * @param mixed $value
   *   The value.
   * @param string $operator
   *   The operator.
   *   Possible values: =, <>, <, <=, >, >=, BETWEEN, NOT BETWEEN,
   *                   IN, NOT IN, IS NULL, IS NOT NULL.
   */
  public function __construct($name, $field, $value, $operator = NULL) {
    // Provide a default based on the data type of the value.
    if (!isset($operator)) {
      $operator = is_array($value) ? 'IN' : '=';
    }
    // Validate the selected operator.
    if (!in_array($operator, self::$supportedOperators)) {
      throw new \InvalidArgumentException(sprintf('Unrecognized operator "%s".', $operator));
    }

    $this->field = $field;
    $this->value = $value;
    $this->operator = $operator;
    parent::__construct($name);
  }

  /**
   * {@inheritdoc}
   */
  public function isDependentOnThisEntity() {
    return TRUE;
  }

  /**
   * Alternate constructor.
   *
   * @param $name
   *   The name.
   * @param string $field
   *   The field, with an optional column name. E.g: 'uid', 'address.locality'.
   * @param mixed $value
   *   The value.
   * @param string $operator
   *   The operator.
   *   Possible values: =, <>, <, <=, >, >=, BETWEEN, NOT BETWEEN,
   *                   IN, NOT IN, IS NULL, IS NOT NULL.
   *
   * @return static
   */
  public static function create($name, $field, $value, $operator = NULL) {
    return new static($name, $field, $value, $operator);
  }

  /**
   * {@inheritdoc}
   */
  public function getConstantValue() {
    return NULL;
  }

  /**
   * Gets the field.
   *
   * @return string
   */
  public function getField() {
    return $this->field;
  }

  /**
   * Gets the value.
   *
   * @return mixed
   */
  public function getValue() {
    return $this->value;
  }

  /**
   * Gets the operator.
   *
   * @return string
   */
  public function getOperator() {
    return $this->operator;
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    if (in_array($this->operator, ['IS NULL', 'IS NOT NULL'])) {
      return "{$this->field} {$this->operator}";
    }
    else {
      if (is_array($this->value)) {
        $value = "['" . implode("', '", $this->value) . "']";
      }
      else {
        $value = "'" . $this->value . "'";
      }

      return "/*{$this->name}*/ {$this->field} {$this->operator} $value";
    }
  }

}
