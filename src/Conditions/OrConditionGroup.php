<?php

namespace Drupal\entity_unified_access\Conditions;

final class OrConditionGroup extends ConditionGroup {

  /**
   * {@inheritDoc}
   */
  public function getConjunction() {
    return 'OR';
  }

  /**
   * @inheritDoc
   */
  public function getEmptyValue() {
    return FALSE;
  }

}
