<?php

namespace Drupal\entity_unified_access\Conditions;

use Drupal\Core\Cache\CacheableDependencyInterface;

/**
 * An Interface for conditions and ConditionGroups.
 */
interface ConditionInterface extends CacheableDependencyInterface {

  /**
   * Gets the name.
   *
   * @return string
   */
  public function getName();

  /**
   * Gets the string representation of the condition for debugging.
   *
   * @return string
   *   The string representation of the condition group.
   */
  public function __toString();

  /**
   * Gets whether the condition depends on the entity.
   *
   * @return bool
   */
  public function isDependentOnThisEntity();

  /**
   * If the condition reduces to a boolean value, return it, otherwise NULL.
   *
   * @return bool|null
   */
  public function getConstantValue();

}
