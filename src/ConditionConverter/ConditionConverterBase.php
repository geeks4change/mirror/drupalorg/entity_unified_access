<?php

namespace Drupal\entity_unified_access\ConditionConverter;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\entity_unified_access\Conditions\ConditionGroup;
use Drupal\entity_unified_access\Conditions\ConditionInterface;
use Drupal\entity_unified_access\Conditions\ConstantCondition;
use Drupal\entity_unified_access\Conditions\FieldCondition;

abstract class ConditionConverterBase {

  /**
   * Whether to simplify the conditions.
   *
   * @var bool
   */
  protected $simplify;

  /**
   * ConditionConverter constructor.
   *
   * @param bool $simplify
   *   Whether to simplify the conditions.
   */
  public function __construct($simplify = TRUE) {
    $this->simplify = $simplify;
  }

  /**
   * Replay conditions.
   *
   * @param \Drupal\entity_unified_access\Conditions\ConditionInterface $condition
   *   The condition.
   * @param mixed|null $resultGroup
   *   The result group that may carry context for creation of new conditions.
   *
   * @return mixed
   *   The result.
   */
  public function convert(ConditionInterface $condition, $resultGroup = NULL) {
    if ($this->simplify && !is_null($condition->getConstantValue())) {
      $result = $this->getConstantCondition($condition, $resultGroup);
    }
    elseif ($condition instanceof ConstantCondition) {
      $result = $this->getConstantCondition($condition, $resultGroup);
    }
    elseif ($condition instanceof FieldCondition) {
      $result = $this->getFieldCondition($condition, $resultGroup);
    }
    elseif ($condition instanceof ConditionGroup) {
      $result = $this->getConditionGroup($condition, $resultGroup);
      foreach ($condition->getConditions($this->simplify) as $subCondition) {
        $subResult = $this->convert($subCondition);
        $result = $this->addConditionToGroup($result, $subResult, $condition, $subCondition);
      }
      $result = $this->finalizeGroup($result, $condition);
    }
    else {
      // We should never get here.
      throw new \UnexpectedValueException('Unexpected condition class.');
    }
    $result = $this->addCacheableDependency($result, $condition);
    if ($condition->isDependentOnThisEntity()) {
      $result = $this->setDependentOnThisEntity($result);
    }
    return $result;
  }

  /**
   * Get constant condition.
   *
   * @param \Drupal\entity_unified_access\Conditions\ConditionInterface $condition
   *   The condition.
   * @param mixed|null $outerResultGroup
   *   The result group that may carry context for creation of new conditions.
   *
   * @return mixed
   *   The result.
   */
  abstract protected function getConstantCondition(ConditionInterface $condition, $outerResultGroup = NULL);

  /**
   * Get field condition.
   *
   * @param \Drupal\entity_unified_access\Conditions\FieldCondition $condition
   *   The condition.
   * @param mixed|null $outerResultGroup
   *   The result group that may carry context for creation of new conditions.
   *
   * @return mixed
   *   The result.
   */
  abstract protected function getFieldCondition(FieldCondition $condition, $outerResultGroup = NULL);

  /**
   * Get condition group.
   *
   * @param \Drupal\entity_unified_access\Conditions\ConditionGroup $group
   *   The condition group.
   * @param mixed|null $outerResultGroup
   *   The result group that may carry context for creation of new conditions.
   *
   * @return mixed
   *   The result.
   */
  abstract protected function getConditionGroup(ConditionGroup $group, $outerResultGroup = NULL);

  /**
   * Add condition to group.
   *
   * @param mixed $resultGroup
   *   The result group.
   * @param mixed $resultCondition
   *   The result condition to add.
   * @param \Drupal\entity_unified_access\Conditions\ConditionGroup $group
   *   The condition group.
   * @param \Drupal\entity_unified_access\Conditions\ConditionInterface $condition
   *   The condition.
   *
   * @return mixed
   *   The result.
   */
  abstract protected function addConditionToGroup($resultGroup, $resultCondition, ConditionGroup $group, ConditionInterface $condition);

  /**
   * FInalize group.
   *
   * @param mixed $resultGroup
   *   The result group.
   * @param \Drupal\entity_unified_access\Conditions\ConditionGroup $group
   *   The condition group.
   *
   * @return mixed
   *   The group with possible final changes.
   */
  abstract protected function finalizeGroup($resultGroup, ConditionGroup $group);

  /**
   * Add cacheable dependency.
   *
   * @param mixed $result
   *   The result.
   * @param \Drupal\Core\Cache\CacheableDependencyInterface $cacheableDependency
   *   The cacheable dependency.
   *
   * @return mixed
   *   The result.
   */
  abstract protected function addCacheableDependency($result, CacheableDependencyInterface $cacheableDependency);

  /**
   * Set dependent on this entity.
   *
   * @param mixed $result
   *   The result.
   *
   * @return mixed
   *   The result.
   */
  abstract protected function setDependentOnThisEntity($result);

}
